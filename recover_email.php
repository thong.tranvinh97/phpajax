<?php 
	include('includes/header.php');
	require_once __DIR__.'/vendor/autoload.php';
?>
	<div class="forgot bg-login">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="forgot-wrap wrap-login">
						<?php  
							include('admin/templates/inc/myconnect.php');
							include('admin/templates/inc/functions.php');
							if($_SERVER['REQUEST_METHOD']=='POST') { 
								$errors = array();
								if (filter_var(($_POST['email']),FILTER_VALIDATE_EMAIL)==TRUE) {
									$email=mysqli_real_escape_string($db->connect(),$_POST['email']);
								}
								else {
									$errors[]='email';
								}
								if (empty($errors)) { 
									$query = "SELECT * FROM $table_prefix WHERE email='{$email}'";
									$results=mysqli_query($db->connect(),$query);kt_query($results,$query);
									if (mysqli_num_rows($results)==1) {
										$key_forgot = md5(time());
										$connect = $db->connect();
										$query_up = "UPDATE $table_prefix 
										SET key_forgot='{$key_forgot}' 
										WHERE email='{$email}'";
										if ($connect->query($query_up) === TRUE) {
											$message = "<p>Sent verification to $email . Please check and activate your new password</p>";
											$mail = new PHPMailer\PHPMailer\PHPMailer(true);
											try {
										    //Server settings
										    // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
										    $mail->isSMTP();                                            // Send using SMTP
										    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
										    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
										    $mail->Username   = 'test.adsplc.97@gmail.com';                     // SMTP username
										    $mail->Password   = 'cubfclpkcfrzynpr';                               // SMTP password
										    // $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
										    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

										    //Recipients
										    $mail->setFrom('test.adsplc.97@gmail.com', 'ADMIN');
										    $mail->addAddress($email, $username);     // Add a recipient
										    // $mail->addAddress('ellen@example.com');               
										    // Name is optional
										    $mail->addReplyTo('test.adsplc.97@gmail.com', 'Information');
										    // $mail->addCC('cc@example.com');
										    // $mail->addBCC('bcc@example.com');

										    // Attachments
										    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
										    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

										    // Content
										    $mail->isHTML(true);                                  // Set email format to HTML
										    $mail->Subject = "Forgot Password";
										    $mail->Body    = "<a href='https://thongtv.lahvui.xyz/phpajax/reset_password.php?key_forgot=$key_forgot'>Recover Account</a>";
										    $mail->AltBody = "This is the body in plain text for non-HTML mail clients";

										    $mail->send();
										    echo '<p style="color: #53ef53f0">Message has been sent</p>';
											} catch (Exception $e) {
											    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
											}
											header('Location: forgot_message.php');
										}
									}
									else {
										$message="<p class='required'>Email is incorrect</p>";
									}
								}
								else {
									$message = "<p class='required'>Please enter your email correctly</p>";	
								}
							}

						?>
						<form class="frmforgot" name="frmforgot" method="POST">
							<h3 class="title">Recover Account</h3>
							<?php if (isset($message)) {
								echo $message;
							} ?>
							<div class="form-group">
								<label>Please enter your email to search for your account.</label>
								<input type="email" name="email" class="form-control" value="" placeholder="Your email">
								<?php if (isset($errors) && in_array('email',$errors)) {
									echo "<p class='required'>Email field is required</p>";
								} ?>
							</div>
							<input type="submit" class="btn btn-primary submit-recover"></input>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include('includes/footer.php'); ?>