<?php 
	include('includes/header.php');
?>
	<div class="register bg-login">
		<div class="container">
			<div class="thankyou">
				<h4>Thank you for register. We have sent a verification email to the address provided</h4>
				<i class="fa fa-envelope-o" aria-hidden="true"></i>
				<a class="back" href="login.php" title=""><i class="fa fa-long-arrow-left" aria-hidden="true"></i>Back</a>
			</div>
		</div>
	</div>
<?php include('includes/footer.php'); ?>