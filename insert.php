<?php 
	include('admin/templates/inc/myconnect.php');
	include('admin/templates/inc/functions.php');
	/*=========ADD USER===========*/
	if($_SERVER['REQUEST_METHOD']=='POST') {
		
		$username = isset($_POST['username']) ? $_POST['username'] : '';
		$password = isset($_POST['password']) ? $_POST['password'] : '';
		$confirm_password = isset($_POST['confirm_password']) ? $_POST['confirm_password'] : '';
		$firstname = isset($_POST['firstname']) ? $_POST['firstname'] : '';
		$lastname = isset($_POST['lastname']) ? $_POST['lastname'] : '';
		$phone = isset($_POST['phone']) ? $_POST['phone'] : '';
		$email = isset($_POST['email']) ? $_POST['email'] : '';
		$birthday = isset($_POST['birthday']) ? $_POST['birthday'] : '';
		$position = isset($_POST['position']) ? $_POST['position'] : '';
		$status = isset($_POST['isactive']) ? $_POST['isactive'] : '';

		$errors = [];
		$status = true;
		$message = 'Create user success!';
		$data = array();

		if(empty($username)) {
			$errors['username'][] = 'Username field is required';
		};
		if(empty($password)) {
			$errors['password'][] = 'Password field is required';
		};
		if($confirm_password != $password) {
			$errors['confirm_password'][] = 'Passwords should be same';
		};
		if(empty($firstname)) {
			$errors['firstname'][] = 'Firstname field is required';
		};
		if(empty($lastname)) {
			$errors['lastname'][] = 'Lastname field is required';
		};
		if(empty($phone)) {
			$errors['phone'][] = 'Phone field is required';
		};
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$errors['email'][] = 'Email field is required';
		};
		if(empty($birthday)) {
			$errors['birthday'][] = 'Birthday field is required';
		};
		if(empty($position)) {
			$errors['position'][] = 'Position field is required';
		};
		$query="SELECT username FROM phpajax_tbluser WHERE username='{$username}'";
		$results=mysqli_query($db->connect(),$query);kt_query($results,$query);
		$query2="SELECT email FROM phpajax_tbluser WHERE email='{$email}'";
		$results2=mysqli_query($db->connect(),$query2);kt_query($results2,$query2);
		if (mysqli_num_rows($results)==1) {
			$errors['username'][] = "Account already exists";
		}
		if (mysqli_num_rows($results2)==1)
		{
			$errors['email'][] = "Email already exists";
		}
		if(!empty($errors)) {
			$status = false;
			$message = 'Create account failed';
			$data =  array(
				'status' => $status,
				'message' => $message,
				'errors' => $errors,
			);
			http_response_code(400);
		}
		else {
			$created = date("Y/m/d");
			$vkey = md5(time());
			$fileLinkdefault = 'upload/image-default.png';
			$connect = $db->connect();
			$query_in="INSERT INTO phpajax_tbluser (username, password, firstname, lastname, phone, email, birthday, role, avatar, fb_id, key_active, key_forgot, is_active, is_deleted, access_token, created_at, updated_at ) VALUES ('{$username}','{$password}',N'{$firstname}',N'{$lastname}','{$phone}','{$email}','{$birthday}','0','{$fileLinkdefault}','','{$vkey}','','0','0','','{$created}','')";
			
			if ($connect->query($query_in) === TRUE) {
				$last_id = $connect->insert_id;
				$query_sq = "SELECT * FROM phpajax_tbluser WHERE id={$last_id}";
				$results_sql = mysqli_query($db->connect(),$query_sq);

				kt_query($results_sql,$query_sq);
				if ($results_sql->num_rows > 0) {
					foreach ($results_sql as $value) {
						$response = $value;

					}
				}
			}
			else {
				$message = "Registration failed";
			}

			$data = array(
				'status' => $status,
				'message' => $message,
				'data' => $response,
			);
			http_response_code(200);
		}
		header("Content-Type: application/json; charset=UTF-8");
		echo json_encode($data);
	}		
?>
