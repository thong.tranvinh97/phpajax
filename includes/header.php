<?php 
  session_start();  
  $url_tg = $_SERVER['REQUEST_URI'];
  $table_prefix = 'phpajax_tbluser';
  if (isset($_GET['code'])) {
    $code = $_GET['code'];
  }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Index</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.css">
    <script src="https://use.fontawesome.com/ec26655b86.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
  <?php  

    if (isset($_SESSION['user']['id']) || isset($_SESSION['access_token']) || isset($code)) {
      include __DIR__.'../../admin/templates/inc/myconnect.php'; 
      if (isset($_SESSION['user']['id'])) {
        $id = $_SESSION['user']['id'];
        $query = "SELECT * FROM $table_prefix WHERE id={$id}";
        $result = mysqli_query($db->connect(),$query);
        if(!$result) {
            die("MySQL error".mysqli_error($db->connect()));
        }
        if(mysqli_num_rows($result)==1) {
            $user = mysqli_fetch_array($result);
        }
        else {
          header('Location: login.php');
          exit();
        }
      }
      
      ?>
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <i class="fa fa-align-right"></i>
            </button>
            <a class="navbar-brand" href="admin/index.php">my<span class="main-color">Home</span></a>
          </div>
          <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hi <?php if(isset($user['username'])) { echo $user['username']; } ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php if($user['role']==1) { echo "admin/profile.php"; } else { echo "profile.php"; } ?> "><i class="fa fa-user-o fw"></i>Profile</a></li>
                  <!-- <li><a href="#"><i class="fa fa-user-o fw"></i> List Users</a></li>
                  <li><a href="#"><i class="fa fa-envelope-o fw"></i> Add New</a></li>
                  <li><a href="#"><i class="fa fa-question-circle-o fw"></i> Help</a></li> -->
                  <li role="separator" class="divider"></li>
                  <li><a href="admin/logout.php?redirect=<?php echo $url_tg; ?>"><i class="fa fa-sign-out"></i> Log out</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <?php
    }
    else {
      ?>
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">WebSiteName</a>
          </div>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="register.php"><i class="fa fa-user" aria-hidden="true"></i>Sign Up</a></li>
            <li><a href="login.php"><i class="fa fa-sign-in" aria-hidden="true"></i>Login</a></li>
          </ul>
        </div>
      </nav>
      <?php
    }
  ?>
    