<?php
	include('admin/templates/inc/myconnect.php');
	include('admin/templates/inc/functions.php');
	$id = $_POST['id'];    
	if (isset($id) && filter_var($id,FILTER_VALIDATE_INT,array('min_range'=>1))) {
        $query = "SELECT * FROM phpajax_tbluser WHERE id={$id}";
        $result = mysqli_query($db->connect(),$query);
        if(!$result) {
            die("MySQL error".mysqli_error($db->connect()));
        }
        if(mysqli_num_rows($result)==1) {
            $user = mysqli_fetch_array($result);
        }
    }

	if($_SERVER['REQUEST_METHOD']=='POST') {
		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$firstname = isset($_POST['firstname']) ? $_POST['firstname'] : '';
		$lastname = isset($_POST['lastname']) ? $_POST['lastname'] : '';
		$phone = isset($_POST['phone']) ? $_POST['phone'] : '';
		$email = isset($_POST['email']) ? $_POST['email'] : '';
		$birthday = isset($_POST['birthday']) ? $_POST['birthday'] : '';
		$position = isset($_POST['position']) ? $_POST['position'] : '';
		$status = isset($_POST['isactive']) ? $_POST['isactive'] : '';

		$errors = [];
		$status = true;
		$message = 'Updated user success!';
		$data = array();
		if(empty($firstname)) {
			$errors['firstname'][] = 'Firstname field is required';
		};
		if(empty($lastname)) {
			$errors['lastname'][] = 'Lastname field is required';
		};
		if(empty($phone)) {
			$errors['phone'][] = 'Phone field is required';
		};
		if(empty($birthday)) {
			$errors['birthday'][] = 'Birthday field is required';
		};


		if (empty($user['fb_id'])) {
            $file = $_FILES['avatar'];
        }
        if(isset($user['avatar'])) {
            $avatar = $user['avatar'];
        }
        else {
            $file = $_FILES['avatar'];
        }
        if($user['password']==md5($_POST['password_current'])) {
            $password_current = $_POST['password_current'];
        }
        else {
            $errors['password_current'] = 'Please enter the current password true';
        }
        $new_password = trim($_POST['new_password']);
        if(strlen($new_password) > 6) {
            $new_password = md5($_POST['new_password']);
        }
        else {
            $errors['new_password']='New password must be more than 6 character';
        }
        if (trim($_POST['confirm_newpassword'])!=trim($_POST['new_password'])) {
            $errors['confirm_newpassword']='Password should be same';
        }
		$updated = date("Y/m/d");
        $vkey = md5(time());
        if(isset($user['fb_id'])) { 
        	$fb_id = $user['fb_id']; 
        };
        if (empty($errors)) {
        	if(empty($fb_id) ) {
	            $fileName = $_FILES['avatar']['name'];
	            $fileTmpName = $_FILES['avatar']['tmp_name'];
	            
	            $fileSize = $_FILES['avatar']['size'];
	            $fileError = $_FILES['avatar']['error'];
	            $fileType = $_FILES['avatar']['type'];
	            $uploads_dir = '/upload';

	            $fileExt = explode('.', $fileName);

	            $fileActualExt = strtolower(end($fileExt));

	            $allowed = array('jpg', 'gif', 'png', 'jpeg');
	            $fileLink = 'upload/'.$fileName;
	            $fileLinkdefault = 'upload/image-default.png';
	            if (in_array($fileActualExt,$allowed)) {
	                if ($fileError === 0) {
	                    if ($fileSize < 10000000) {
	                        $fileNameNew = uniqid('', true).".".$fileActualExt;
	                        define ('SITE_ROOT', dirname(__DIR__, 1));
	                        $fileDestination = SITE_ROOT.'/phpajax/'.$fileLink;
	                        move_uploaded_file($fileTmpName, $fileDestination);
	                    }
	                    else {
	                        $message = "Your file is too big!";
	                    }
	                }
	                else {
	                    $message = "There was an error uploading your file!";
	                }
	            }
	            if(!empty($fileName) && $fileName!=='') {
	                $query_up="UPDATE phpajax_tbluser
	                    SET password='{$new_password}',
	                    	firstname='{$firstname}',
	                        lastname='{$lastname}',
	                        phone='{$phone}',
	                        email='{$email}', 
	                        birthday='{$birthday}', 
	                        avatar='{$fileLink}', 
	                        is_active={$status},
	                        updated_at={$updated}
	                    WHERE id={$id} 
	                ";

	            }
	            else {
	                if (empty($avatar)) {
	                    $query_up="UPDATE phpajax_tbluser
	                    SET password='{$new_password}',
	                    	firstname='{$firstname}',
	                        lastname='{$lastname}',
	                        phone='{$phone}',
	                        email='{$email}', 
	                        birthday='{$birthday}', 
	                        avatar='{$fileLinkdefault}', 
	                        is_active={$status},
	                        updated_at={$updated}
	                    WHERE id={$id} 
	                ";
	                }
	                else {
	                    $query_up="UPDATE phpajax_tbluser
	                        SET password='{$new_password}',
	                        	firstname='{$firstname}',
	                            lastname='{$lastname}',
	                            phone='{$phone}',
	                            email='{$email}', 
	                            birthday='{$birthday}', 
	                            avatar='{$avatar}', 
	                            is_active={$status},
	                            updated_at={$updated}
	                        WHERE id={$id} 
	                    ";
	                }
	            }
	            $results_up=mysqli_query($db->connect(),$query_up);
	            if(!$results_up) {
	                die("MySQL error".mysqli_error($db->connect()));
	            }
	            else {
	                $query_laup = "SELECT * FROM phpajax_tbluser WHERE id={$id}";
	                $result_laup = mysqli_query($db->connect(),$query_laup);
	                if(!$result_laup) {
	                    die("MySQL error".mysqli_error($db->connect()));
	                }
	                if(mysqli_num_rows($result_laup)==1) {
	                    foreach ($result_laup as $value) {
							$response = $value;
						}
	                }
	            }
	            $data = array(
					'status' => $status,
					'message' => $message,
					'data' => $response,
				);
	        }
	        else   
	        {
	            $query_up="UPDATE phpajax_tbluser
	                    SET password='{$new_password}',
	                    	firstname='{$firstname}',
	                        lastname='{$lastname}',
	                        phone='{$phone}',
	                        email='{$email}', 
	                        birthday='{$birthday}', 
	                        avatar='{$avatar}', 
	                        is_active={$isactive},
	                        updated_at={$updated}
	                    WHERE id={$id} 
	                ";
	            $results_up=mysqli_query($db->connect(),$query_up);
	            if(!$results_up) {
	                die("MySQL error".mysqli_error($db->connect()));
	            }
	            if (mysqli_affected_rows($db->connect())==1) {

	                echo "<p class='required'>You have not fixed anything</p>";
	            }
	            else {
	                $query_laup = "SELECT * FROM phpajax_tbluser WHERE id={$id}";
	                $result_laup = mysqli_query($db->connect(),$query_laup);
	                if(!$result_laup) {
	                    die("MySQL error".mysqli_error($db->connect()));
	                }
	                if(mysqli_num_rows($result_laup)==1) {
	                    foreach ($result_laup as $value) {
							$response = $value;
						}
	                }
	            }
	            $data = array(
					'status' => $status,
					'message' => $message,
					'data' => $response,
				);
	        } 
	        http_response_code(200);
        }
		else {
			$status = false;
			$message = 'Updated account failed';
			$data =  array(
				'status' => $status,
				'message' => $message,
				'errors' => $errors,
			);
			http_response_code(400);
		}
		echo json_encode($data);
	} 
?>