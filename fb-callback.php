<?php
    require_once __DIR__."/vendor/autoload.php"; 
    require_once("fbconfig.php");

    try {
        $accessToken = $helper->getAccessToken();
    } catch (\Facebook\Exceptions\FacebookResponseException $e) {
        echo "Graph Exception: " . $e->getMessage();
        exit();
    } catch (\Facebook\Exceptions\FacebookSDKException $e) {
        echo "SDK Exception: " . $e->getMessage();
        exit();
    }

    if (!$accessToken) {
        header('Location: login.php');
        exit();
    }

    if (! isset($accessToken)) {
      if ($helper->getError()) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Error: " . $helper->getError() . "\n";
        echo "Error Code: " . $helper->getErrorCode() . "\n";
        echo "Error Reason: " . $helper->getErrorReason() . "\n";
        echo "Error Description: " . $helper->getErrorDescription() . "\n";
      } else {
        header('HTTP/1.0 400 Bad Request');
        echo 'Bad request';
      }
      exit;
    }

    try {
        $response = $FB->get('/me?fields=id, first_name, last_name, email, picture.type(large)', $accessToken->getValue());
    } catch (Facebook\Exceptions\FacebookResponseException $e) {
        echo "Graph returned an error: " . $e->getMessage();
        exit();
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        echo "Facebook SDK returned an error: " . $e->getMessage();
        exit();
    }

    $fbUser = $response->getGraphNode()->asArray();
    $_SESSION['access_token'] = (string) $accessToken;
    if(!empty($fbUser)) {
        include('admin/templates/inc/myconnect.php');
        include('admin/templates/inc/functions.php');
        $fb_id = $fbUser['id'];
        $first_name = $fbUser['first_name'];
        $last_name = $fbUser['last_name'];
        $email = $fbUser['email'];
        $email_string = explode('@',$email);
        $image = $fbUser['picture']['url'];
        $username = $email_string[0];
        $created = date("Y/m/d");
        $vkey = md5(time());
        $access_token = $_SESSION['access_token'];
        $query_s="SELECT * FROM phpajax_tbluser WHERE email='{$email}'";
        $results_s=mysqli_query($db->connect(),$query_s);
        kt_query($results_s, $query_s);
        if (mysqli_num_rows($results_s)==1) {
            $rows=mysqli_fetch_array($results_s);
            $_SESSION['user']=$rows;    
            header('Location: index.php');
        }
        else {
            $query_in="INSERT INTO phpajax_tbluser (username, password, firstname, lastname, phone, email, birthday, role, avatar, fb_id, key_active, key_forgot, is_active, is_deleted, access_token, created_at, updated_at) VALUES ('{$username}','','{$first_name}','{$last_name}','','{$email}','','0','{$image}','{$fb_id}','','', '1','0','{$access_token}','','')";

            $results_in=mysqli_query($db->connect(),$query_in);

            kt_query($results_in, $query_in);
            if (mysqli_affected_rows($db->connect())==1) {
                echo "<p class='required'>Đăng kí không thành công</p>";
                header('Location: login.php?redirect='.$url_tg);
            }
            else {
                echo "<p style='color: #53ef53f0;'>Đăng kí thành công.</p>";
                header('Location: index.php?redirect='.$url_tg);
            }
        }
    }
   
?>