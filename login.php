<?php 
	include ('includes/header.php'); 
	require_once __DIR__.'/fbconfig.php';
	require_once __DIR__.'/ggconfig.php';
	
	
	
	if (isset($_SESSION['user']['id']) AND $_SESSION['user']['role']==='1' || isset($_SESSION['access_token']) AND $_SESSION['user']['role']==='1' || isset($token) AND $_SESSION['user']['role']==='1') {
	    header('Location: admin/index.php');
	}
	elseif (isset($_SESSION['user']['id']) AND $_SESSION['user']['role']==='0' || isset($_SESSION['access_token']) AND $_SESSION['user']['role']==='0' || isset($token) AND $_SESSION['user']['role']==='0') {
	    header('Location: index.php');
	}
	
	$redirecURL = "https://thongtv.lahvui.xyz/phpajax/fb-callback.php";
	$permissions = ['email'];
	$loginURL = $helper->getLoginUrl($redirecURL, $permissions);

?>
	<div class="login bg-login">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="wrap-login">
						<h2 class="title">Account Login</h2>
						<?php  
							include __DIR__.('/admin/templates/inc/myconnect.php');
							include __DIR__.('/admin/templates/inc/functions.php');
							if($_SERVER['REQUEST_METHOD']=='POST') 
							{
								$errors=array();
								if(empty($_POST['username'])) {
									$errors[]='username';
								}
								else {
									$username=$_POST['username'];
								}
								if(empty($_POST['password'])) {
									$errors[]='password';
								}
								else {
									$password=md5($_POST['password']);
								}	
								if(isset($_POST['g-recaptcha-response'])){
						          $captcha=$_POST['g-recaptcha-response'];
						        }
						        else {
						        	$errors[]='recaptcha';
						        }
								if(!$captcha){
							        $message = 'Please check form captcha';
						        }
						        $secretKey = "6LebisgZAAAAAKoJ_BHa7Z1Lbs3cXkUJnAFkEXn8";
						        $ip = $_SERVER['REMOTE_ADDR'];
						        // post request to server
						        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
						        $response = file_get_contents($url);
						        $responseKeys = json_decode($response,true);
						        // should return JSON with success as true
						        if($responseKeys["success"]) {
					                if (empty($errors)) {
										$query="SELECT * FROM $table_prefix WHERE username='{$username}' OR email='{$username}' AND password='{$password}' AND is_active=1";
										$result=mysqli_query($db->connect(),$query);
										kt_query($result,$query);
										if (mysqli_num_rows($result)==1) {
											$rows=mysqli_fetch_array($result);
											$_SESSION['user']=$rows;	
											if(!empty($_POST['remember'])) {
												setcookie("member_login",$_POST['username'],time()+ (10 * 365 * 24 * 60 * 60));
												setcookie("member_password",$_POST['password'],time()+ (10 * 365 * 24 * 60 * 60));
												setcookie("member_check",$_POST['remember'],time()+ (10 * 365 * 24 * 60 * 60));
											}
											else {
												if(isset($_COOKIE["member_login"])) {
													setcookie("member_login","");
												}
												if(isset($_COOKIE["member_password"])) {
													setcookie("member_password","");
												}
												if(isset($_COOKIE["member_check"])) {
													setcookie("member_check","");
												}
											}
											$url = $_GET['redirect'];
											if (isset($url)) {
												header('Location:'.$url);	
											}
											else {
												header('Location: index.php');	
											}
										}
										else 
										{
											$message="<p class='required'>The account or password is incorrect</p>";
										}
									}
						        } 
							}
						?>
						<form class="frmlogin" name="frmlogin" method="POST">
							<div class="invalid-feedback has-error align-center">
								<div class="help-block with-errors"><?php if(isset($message)) { echo $message; } ?></div>
							</div>
							<div class="form-group">
								<label>Username</label>
								<input type="text" name="username" class="form-control" value="<?php if(isset($_COOKIE['member_login'])) { echo $_COOKIE['member_login']; } ?>" placeholder="Enter Username">
								<?php if (isset($errors) && in_array('username',$errors)) {
									echo "<p class='required'>Username field is required</p>";
								} ?>
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" name="password" class="form-control" value="<?php if(isset($_COOKIE['member_password'])) { echo $_COOKIE['member_password']; } ?>" placeholder="Enter Password">
								<?php if (isset($errors) && in_array('password',$errors)) {
									echo "<p class='required'>Password field is required</p>";
								} ?>
							</div>
							<div class="form-group remem-field">
								<?php 
									if(isset($_COOKIE['member_check'])) {
									?>
									<input type="checkbox" name="remember" value="lsRememberMe" id="rememberMe" checked>
									<label for="rememberMe">Remember me</label>
									<?php
									}
									else {
									?>
									<input type="checkbox" name="remember" value="lsRememberMe" id="rememberMe">
									<label for="rememberMe">Remember me</label>
									<?php
									}
								?>
								 
							</div>
							<div class="g-recaptcha" data-sitekey="6LebisgZAAAAAGIgEHGyNeyPjpY6DoGg3qxogQKb"></div>
							<br>
							<div class="form-group login-field">
								<button type="submit" id="btn-login" class="btn btn-primary">Login</button>
								<span style="padding: 0 5px;">or</span>
								<button type="button" onclick="fbLogin()" class="btn btn-primary btn-fb">Sign in with Facebook</button>
								<span style="padding: 0 5px;">or</span>
								<!-- <button type="button" onclick="<?php $client->createAuthUrl(); ?>" class="btn btn-danger btn-google">Sign in with Google</button> -->
								<div class="btn-google">
									<?php echo $output; ?>
								</div>
							</div>
							<div class="form-group remem-field">
								<a href="recover_email.php" class="forgot" title="">Forgot your password ?</a>
								<a href="register.php" class="register" title="">Register</a>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		window.fbAsyncInit = function() {
    FB.init({
      appId      : '1702823796565334',
      cookie     : true,
      xfbml      : true,
      version    : 'v8.0'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  function fbLogin() {
  	FB.login(function(response) {
  		if (response.authResponse) {
  			fbAfterLogin();
  		}
  	});
  }

  function fbAfterLogin() {
  	FB.getLoginStatus(function(response) {
  		if (response.status === 'connected') {
  			window.location = '<?php echo $loginURL; ?>';
		}
	});
  }
	</script>
<?php include ('includes/footer.php'); ?>