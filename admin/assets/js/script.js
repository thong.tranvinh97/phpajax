jQuery(document).ready(function($) {
	$('.datepickerbd').datepicker({
		dateFormat: "yy-mm-dd",
		maxDate: new Date(2002, 1, 1)
	});
	$('#formadd').on('submit',function(e) {
		e.preventDefault();
		forms = $('#formadd');
		$.ajax({
			url: "../insert.php",
			method: "post",
			data: $('form#formadd').serialize(),
			dataType: "json",
			success: function(result) {
				forms.find('.message.help-block').addClass('success');
				forms.find('.message.help-block').text(result.message);
				forms.each(function(){
	                this.reset(); 
	                $(this).find('.form-group').removeClass('has-error');
	                $(this).find('.form-group .help-block').text('');
	            });
			},
			error: function(response) {
				var status = response.responseJSON.status;
				var message = response.responseJSON.message;
				form_errors = response.responseJSON.errors;
				if(status === false) {
					forms.find('.message.help-block').removeClass('success');
					forms.find('.message.help-block').addClass('has-error');
					forms.find('.message.help-block.with-errors').text(message);
					forms.find('.form-group').addClass('has-error');
				}
				else {
					forms.find('.message.help-block').removeClass('has-error');
					forms.find('.message.help-block').addClass('success');
					forms.find('.message.help-block.with-errors').text(message);
					forms.find('.form-group').removeClass('has-error');
				}
				forms.find('.username.with-errors').text(form_errors.username);
				forms.find('.password.with-errors').text(form_errors.password);
				forms.find('.confirm_password.with-errors').text(form_errors.confirm_password);
				forms.find('.firstname.with-errors').text(form_errors.firstname);
				forms.find('.lastname.with-errors').text(form_errors.lastname);
				forms.find('.phone.with-errors').text(form_errors.phone);
				forms.find('.email.with-errors').text(form_errors.email);
				forms.find('.birthday.with-errors').text(form_errors.birthday);
				forms.find('.position.with-errors').text(form_errors.position);
			}
		});
	});
	var userdata = $('#user-data');
	userdata.on('click','.deleteUser',function(e) {
		e.preventDefault();
		var id = $(this).data('id');
		var td = $(this).parents('td');
		var tr = td.closest('tr');
		bootbox.confirm("Do you want delete user?", function(result) {
			if(result) {
				$.ajax({
					url: '../delete.php',
					type: 'post',
					data: {
						id : id,
					},
					dataType: "json",
					success:function(response) {
						tr.fadeOut(1000, function(){
	                        $(this).remove();
	                    });
					},
					error:function(response) {
						console.log('bbbbbb');
					}
				});
			}
		});
	});
	var formupdate = $('form#formupdate');
	formupdate.on('submit',function(e) {
		e.preventDefault();
		var myfileinput = $('#file');
		var formData = new FormData(this);
		formData.append('image', $('input[type=file]')[0].files[0]);
		var id = formupdate.attr('data-id');
		var tr = formupdate.closest('tr'); 
		$.ajax({
			url: '../update.php',
			type: 'post',
			cache: false,
			processData: false,
    		contentType: false,
			data: formData,
			dataType: "json",
			success:function(response) {
				formupdate.find('.message.help-block').addClass('success');
				formupdate.find('.message.help-block').text(response.message);
				var oldSrc = formupdate.find('#avatarUser').attr('src');
				var newSrc = response.data.avatar;
				var path = $(location).prop("href").split("/").slice(0,-2).join("/").concat("/",newSrc);
				formupdate.find('#avatarUser').attr('src',path);
			},
			error: function(response) {
				var status = response.responseJSON.status;
				var message = response.responseJSON.message;
				form_errors = response.responseJSON.errors;
				if(status === false) {
					formupdate.find('.message.help-block').removeClass('success');
					formupdate.find('.message.help-block').addClass('has-error');
					formupdate.find('.message.help-block.with-errors').text(message);
				}
				else {
					formupdate.find('.message.help-block').removeClass('has-error');
					formupdate.find('.message.help-block').addClass('success');
					formupdate.find('.message.help-block.with-errors').text(message);
					formupdate.find('.form-group').removeClass('has-error');
				}
				formupdate.find('.firstname.with-errors').text(form_errors.firstname);
				formupdate.find('.lastname.with-errors').text(form_errors.lastname);
				formupdate.find('.phone.with-errors').text(form_errors.phone);
				formupdate.find('.birthday.with-errors').text(form_errors.birthday);
		    }
		});
	});	

	var pfupdate = $('form#pfupdate');
	pfupdate.on('submit',function(e) {
		e.preventDefault();
		var myfileinput = $('#file');
		var formData = new FormData(this);
		var formDataimage = formData.append('image', $('input[type=file]')[0].files[0]);
		var id = pfupdate.attr('data-id');
		var tr = pfupdate.closest('tr'); 
		var abc = formData.append('id', id);
		$.ajax({
			url: '../update-profile.php',
			type: 'post',
			cache: false,
			processData: false,
    		contentType: false,
			data: formData,
			dataType: "json",
			success:function(response) {
				console.log(response);
				pfupdate.find('.message.help-block').addClass('success');
				pfupdate.find('.message.help-block').text(response.message);
				var oldSrc = pfupdate.find('#avatarUser').attr('src');
				var newSrc = response.data.avatar;
				var path = $(location).prop("href").split("/").slice(0,-2).join("/").concat("/",newSrc);
				pfupdate.find('#avatarUser').attr('src',path);
			},
			error: function(response) {
				console.log(response.responseJSON);
				var status = response.responseJSON.status;
				var message = response.responseJSON.message;
				form_errors = response.responseJSON.errors;
				if(status === false) {
					pfupdate.find('.message.help-block').removeClass('success');
					pfupdate.find('.message.help-block').addClass('has-error');
					pfupdate.find('.message.help-block.with-errors').text(message);
				}
				else {
					pfupdate.find('.message.help-block').removeClass('has-error');
					pfupdate.find('.message.help-block').addClass('success');
					pfupdate.find('.message.help-block.with-errors').text(message);
					pfupdate.find('.form-group').removeClass('has-error');
				}
				pfupdate.find('.firstname.with-errors').text(form_errors.firstname);
				pfupdate.find('.lastname.with-errors').text(form_errors.lastname);
				pfupdate.find('.phone.with-errors').text(form_errors.phone);
				pfupdate.find('.birthday.with-errors').text(form_errors.birthday);
				pfupdate.find('.new_password.with-errors').text(form_errors.new_password);
				pfupdate.find('.password_current.with-errors').text(form_errors.password_current);
				pfupdate.find('.confirm_newpassword.with-errors').text(form_errors.confirm_newpassword);
		    }
		});
	});	

	var pfupdatefe = $('form#pfupdatefe');
	pfupdatefe.on('submit',function(e) {
		e.preventDefault();
		var myfileinput = $('#file');
		var formData = new FormData(this);
		formData.append('image', $('input[type=file]')[0].files[0]);
		var id = pfupdatefe.attr('data-id');
		var tr = pfupdatefe.closest('tr'); 
		formData.append('id', id);
		console.log(formData);
		$.ajax({
			url: 'update-profile.php',
			type: 'post',
			cache: false,
			processData: false,
    		contentType: false,
			data: formData,
			dataType: "json",
			success:function(response) {
				pfupdatefe.find('.message.help-block').addClass('success');
				pfupdatefe.find('.message.help-block').text(response.message);
				var oldSrc = pfupdatefe.find('#avatarUser').attr('src');
				var newSrc = response.data.avatar;
				var path = $(location).prop("href").split("/").slice(0,-1).join("/").concat("/",newSrc);
				pfupdatefe.find('#avatarUser').attr('src',path);
			},
			error: function(response) {
				var status = response.responseJSON.status;
				var message = response.responseJSON.message;
				form_errors = response.responseJSON.errors;
				console.log(response);
				if(status === false) {
					pfupdatefe.find('.message.help-block').removeClass('success');
					pfupdatefe.find('.message.help-block').addClass('has-error');
					pfupdatefe.find('.message.help-block.with-errors').text(message);
				}
				else {
					pfupdatefe.find('.message.help-block').removeClass('has-error');
					pfupdatefe.find('.message.help-block').addClass('success');
					pfupdatefe.find('.message.help-block.with-errors').text(message);
					pfupdatefe.find('.form-group').removeClass('has-error');
				}
				pfupdatefe.find('.firstname.with-errors').text(form_errors.firstname);
				pfupdatefe.find('.lastname.with-errors').text(form_errors.lastname);
				pfupdatefe.find('.phone.with-errors').text(form_errors.phone);
				pfupdatefe.find('.birthday.with-errors').text(form_errors.birthday);
		    }
		});
	});	
});