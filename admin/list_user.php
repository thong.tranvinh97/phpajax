<?php 
    include('templates/header.php'); 
?>
<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <section id="contents">
                    <div class="ql-user">
                        <h3>Quản lí User</h3>
                        <div id="user-data">
                            <table class="table table-bordered" id="crud_table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Username</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Birthday</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php  
                                        include('templates/inc/myconnect.php');
                                        include('templates/inc/functions.php');
                                        $query="SELECT * FROM $table_prefix ORDER BY id DESC";
                                        $results=mysqli_query($db->connect(),$query);
                                        kt_query($results,$query);
                                        while($user=mysqli_fetch_array($results,MYSQLI_ASSOC))
                                        {
                                        ?>
                                            <tr>
                                                <td><?php echo $user['id']; ?></td>
                                                <td><?php echo $user['username']; ?></td>
                                                <td><?php echo $user['firstname']; ?></td>
                                                <td><?php echo $user['lastname']; ?></td>
                                                <td><?php echo $user['phone']; ?></td>
                                                <td><?php echo $user['email']; ?></td>
                                                <td><?php echo $user['birthday']; ?></td>
                                                <td>
                                                    <?php 
                                                        if(isset($user['role'])==0){
                                                            echo 'Admin'; 
                                                        }
                                                        else {
                                                            echo "Member";
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        if(isset($user['is_active'])==0){
                                                            echo 'Deactivate'; 
                                                        }
                                                        else {
                                                            echo "Active";
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a href="edit_user.php?id=<?php echo $user['id']; ?>" class="updateUser"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                </td>
                                                <td><a href="delete_user.php?id=<?php echo $user['id']; ?>" class="deleteUser" data-id="<?php echo $user['id']; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                            </tr>
                                        <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        
    </div>
</div>
<?php include('templates/footer.php'); ?>