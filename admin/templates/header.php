<?php  
  session_start();
  $url_tg = $_SERVER['REQUEST_URI'];
  $table_prefix = 'phpajax_tbluser';
  if (!isset($_SESSION['user']['id']) AND !isset($_SESSION['access_token'])) {
      header('Location: ../login.php?redirect='.$url_tg);
  }
  elseif (isset($_SESSION['user']['id']) AND $_SESSION['user']['role']=='0' || isset($_SESSION['access_token']) AND $_SESSION['user']['id'] AND $_SESSION['user']['role']=='0') {
      header('Location: ../index.php');
  }
  include __DIR__.'../../templates/inc/myconnect.php';
  $id = $_SESSION['user']['id'];
  $query = "SELECT * FROM $table_prefix WHERE id={$id}";
  $result = mysqli_query($db->connect(),$query);
  if(!$result) {
      die("MySQL error".mysqli_error($db->connect()));
  }
  if(mysqli_num_rows($result)==1) {
      $user = mysqli_fetch_array($result);
  }
  else {
    header('Location: ../login.php');
    exit();
  }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
  
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <i class="fa fa-align-right"></i>
            </button>
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle main-color" data-toggle="dropdown">my <span>Dashboard</span></a>
                <ul class="dropdown-menu">
                    <li><a href="../index.php">Home</a></li>
                    <li><a href="index.php">Dashboard</a></li>
                </ul>
              </li>
            </ul>
          </div>
          <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php if ($user['role']=='1') 
                {
                  ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Users <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="list_user.php">List Users</a></li>
                            <li><a href="add_user.php">Add User</a></li>
                        </ul>
                    </li>
                  <?php
                } 
                ?>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hi <?php if(isset($user['username'])) { echo $user['username']; } ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="profile.php"><i class="fa fa-user-o fw"></i> My Profile</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="logout.php?redirect=<?php echo $url_tg; ?>"><i class="fa fa-sign-out"></i> Log out</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>