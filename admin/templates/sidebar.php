<?php 
    include __DIR__.'/inc/myconnect.php';
    $id=$_SESSION['user']['id'];

    $query = "SELECT * FROM tbluser WHERE id={$id}";

    $result = mysqli_query($db->connect(),$query);
    if(!$result) {
        die("MySQL error".mysqli_error($db->connect()));
    }
    if(mysqli_num_rows($result)==1) {
        $user = mysqli_fetch_array($result);
    }
    
?>
<aside class="side-nav" id="show-side-navigation1">
    <i class="fa fa-bars close-aside hidden-sm hidden-md hidden-lg" data-close="show-side-navigation1"></i>
    <div class="heading">
        <?php 
            if (($user['fb_id'])!='null') {
            ?>
                <img src="<?php if (isset($user['avatar'])) { echo $user['avatar']; }?>" alt="">
            <?php
            } 
            else {
            ?>
                <img src="http://127.0.0.1/testphp/admin/assets/images/image-default.png" alt="">
            <?php
            }
        ?>
        
    </div>
    <div class="info">
        <h3>
            <a href="javascript:void(0)">
                <?php 
                    if (($user['fb_id'])=='null') {
                        echo $_SESSION['user']['username'];
                    } 
                    else 
                    {
                        echo $user['firstname'].' '.$user['lastname'];
                    }
                ?>
            </a>
        </h3>
        <p>Lorem ipsum dolor sit amet consectetur.</p>
    </div>
    <ul class="categories">
        <li><a href="add_user.php">Add New</a></li>
        <li><a href="index.php">List User</a></li>
    </ul>
</aside>