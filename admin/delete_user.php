<?php  
	include('templates/header.php');
	include('templates/inc/myconnect.php');
	include('templates/inc/functions.php');
	if (isset($_GET['id']) && filter_var($_GET['id'],FILTER_VALIDATE_INT,array('min_range'=>1))) {
	    $id = $_GET['id'];
	    $query = "DELETE FROM $table_prefix WHERE id={$id}";
	    $results=mysqli_query($db->connect(),$query);
        kt_query($results,$query);
        header('Location: list_user.php');
	}
	else {
	    header('Location: index.php');
	}
?>