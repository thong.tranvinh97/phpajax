<?php 
    include('templates/header.php'); 
    include('templates/inc/myconnect.php');
    include('templates/inc/functions.php');
    if($_SERVER['REQUEST_METHOD']=='POST') {
        $errors = array();
        if (empty($_POST['username'])) {
            $errors[]='username';
        }
        else {
            $username=$_POST['username'];
        }
        if (empty($_POST['password'])) {
            $errors[]='password';
        }
        else {
            $password=md5(trim($_POST['password']));
        }
        if (trim($_POST['confirm_password'])!=trim($_POST['password'])) {
            $errors[]='confirm_password';
        }
        if (empty($_POST['firstname'])) {
            $errors[]='firstname';
        }
        else {
            $firstname=$_POST['firstname'];
        }
        if (empty($_POST['lastname'])) {
            $errors[]='lastname';
        }
        else {
            $lastname=$_POST['lastname'];
        }
        if (empty($_POST['phone'])) {
            $errors[]='phone';
        }
        else {
            $phone=$_POST['phone'];
        }
        if (filter_var(($_POST['email']),FILTER_VALIDATE_EMAIL)==TRUE) {
            $email=mysqli_real_escape_string($db->connect(),$_POST['email']);
        }
        else {
            $errors[]='email';
        }
        if (empty($_POST['birthday'])) {
            $errors[]='birthday';
        }
        else {
            $birthday=$_POST['birthday'];
        }
        $isactive = $_POST['isactive'];
        if(empty($errors)) {
            $query="SELECT username FROM phpajax_tbluser WHERE username='{$username}'";
            $results=mysqli_query($db->connect(),$query);kt_query($results,$query);
            $query2="SELECT email FROM phpajax_tbluser WHERE email='{$email}'";
            $results2=mysqli_query($db->connect(),$query2);kt_query($results2,$query2);
            if (mysqli_num_rows($results)==1) {
                $message="<p class='required'>Account already exists</p>";
            }
            elseif (mysqli_num_rows($results2)==1)
            {
                $message="<p class='required'>Email already exists</p>";
            }
            else {

                $created = date("Y/m/d");
                $vkey = md5(time());
                $fileLinkdefault = 'upload/image-default.png';
                $query_in="INSERT INTO phpajax_tbluser (username, password, firstname, lastname, phone, email, birthday, role, avatar, fb_id, key_active, key_forgot, is_active, is_deleted, access_token, created_at, updated_at) VALUES ('{$username}','{$password}',N'{$firstname}',N'{$lastname}','{$phone}','{$email}','{$birthday}','0','{$fileLinkdefault}','','{$vkey}','','0','0','','{$created}','')";
                $results_in=mysqli_query($db->connect(),$query_in);
                kt_query($results_in, $query_in);
                if (mysqli_affected_rows($db->connect())==1) {
                    echo "<p class='required'>Đăng kí không thành công</p>";
                }
                else {
                    echo "<p style='color: #53ef53f0;'>Đăng kí thành công.</p>";

                }
            }
        }
        else {
            $message="<p class='required'>Please enter your info full </p>";
        }
    }
?>
<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <section id="contents">
                    <div class="ql-user">
                        <h3>Add User</h3>
                        <form id="formadd" name="formadd" method="post" action="add_user.php" data-toggle="validator" novalidate="novalidate" role="form">
                            <div class="message help-block with-errors"></div>
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" name="username" class="form-control username" value="" required>
                                <div class="help-block username with-errors"></div>
                                <?php if (isset($errors) && in_array('username',$errors)) {
                                    echo "<p class='required'>Username field is required</p>";
                                } ?>
                            </div>
                            <div class="form-group field_password">
                                <label>Password</label>
                                <input type="password" name="password" class="form-control password" value="">
                                <div class="help-block password with-errors"></div>
                                <?php if (isset($errors) && in_array('password',$errors)) {
                                    echo "<p class='required'>Password field is required</p>";
                                } ?>
                            </div>
                            <div class="form-group field_password">
                                <label>Confirm Password</label>
                                <input type="password" name="confirm_password" class="form-control confirm_password" value="">
                                <div class="help-block confirm_password with-errors"></div>
                                <?php if (isset($errors) && in_array('confirm_password',$errors)) {
                                    echo "<p class='required'>Passwords should be same</p>";
                                } ?>
                            </div>
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="firstname" class="form-control firstname" value="">
                                <div class="help-block firstname with-errors"></div>
                                <?php if (isset($errors) && in_array('firstname',$errors)) {
                                    echo "<p class='required'>First Name field is required</p>";
                                } ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="lastname" class="form-control lastname" value="">
                                <div class="help-block lastname with-errors"></div>
                                <?php if (isset($errors) && in_array('lastname',$errors)) {
                                    echo "<p class='required'>Last Name field is required</p>";
                                } ?>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="number" name="phone" class="form-control phone" value="">
                                <div class="help-block phone with-errors"></div>
                                <?php if (isset($errors) && in_array('phone',$errors)) {
                                    echo "<p class='required'>Phone field is required</p>";
                                } ?>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control email" value="">
                                <div class="help-block email with-errors"></div>
                                <?php if (isset($errors) && in_array('email',$errors)) {
                                    echo "<p class='required'>Email field is required</p>";
                                } ?>
                            </div>
                            <div class="form-group">
                                <label>Birthday</label>
                                <input type="date" name="birthday" class="form-control datepickerbd birthday" value="">
                                <div class="help-block birthday with-errors"></div>
                                <?php if (isset($errors) && in_array('birthday',$errors)) {
                                    echo "<p class='required'>Birth Day field is required</p>";
                                } ?>
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <input type="text" name="position" class="form-control position" value="">
                                <div class="help-block position with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label style="display: block">Status</label>
                                <label class="radio-inline"><input checked="checked" type="radio" class="isactive" name="isactive" value="1">Active</label>
                                <label class="radio-inline"><input type="radio" class="isactive" name="isactive" value="0">Deactive</label> 
                            </div>
                            <input name="submit" type="submit" id="addRecord" class="btn btn-primary" value="Submit">
                        </form>
                   </div>
                </section>
            </div>
        </div>
    </div>
</div>
<?php include('templates/footer.php'); ?>