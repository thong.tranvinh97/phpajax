<?php 
    include('templates/header.php'); 
    include('templates/inc/myconnect.php');
    $id = $_GET['id'];
    if (isset($id) && filter_var($id,FILTER_VALIDATE_INT,array('min_range'=>1))) {
        $query = "SELECT * FROM $table_prefix WHERE id={$id}";
        $result = mysqli_query($db->connect(),$query);
        if(!$result) {
            die("MySQL error".mysqli_error($db->connect()));
        }
        if(mysqli_num_rows($result)==1) {
            $user = mysqli_fetch_array($result);
        }
    }
    if($_SERVER['REQUEST_METHOD']=='POST') {
        $id = $user['id'];
        $errors = array();
        if (empty($_POST['username'])) {
            $errors[]='username';
        }
        else {
            $username=$_POST['username'];
        }
        if (empty($_POST['firstname'])) {
            $errors[]='firstname';
        }
        else {
            $firstname=$_POST['firstname'];
        }
        if (empty($_POST['lastname'])) {
            $errors[]='lastname';
        }
        else {
            $lastname=$_POST['lastname'];
        }
        if (filter_var(($_POST['email']),FILTER_VALIDATE_EMAIL)==TRUE) {
            $email=mysqli_real_escape_string($db->connect(),$_POST['email']);
        }
        else {
            $errors[]='email';
        }
        if (empty($user['fb_id'])) {
            $file = $_FILES['avatar'];
        }
        if(isset($user['avatar'])) {
            $avatar = $user['avatar'];
        }
        else {
            $file = $_FILES['avatar'];
        }
        $birthday=$_POST['birthday'];
        $avatar = $user['avatar'];
        $phone=$_POST['phone'];
        $isactive = $_POST['isactive'];
        $updated = date("Y/m/d");
        $vkey = md5(time());
        $fb_id = $user['fb_id'];
        if(empty($errors)) {
            if(empty($fb_id)) {
                $fileName = $_FILES['avatar']['name'];

                $fileTmpName = $_FILES['avatar']['tmp_name'];
                
                $fileSize = $_FILES['avatar']['size'];
                $fileError = $_FILES['avatar']['error'];
                $fileType = $_FILES['avatar']['type'];
                $uploads_dir = '/upload';

                $fileExt = explode('.', $fileName);

                $fileActualExt = strtolower(end($fileExt));

                $allowed = array('jpg', 'gif', 'png', 'jpeg');
                $fileLink = 'upload/'.$fileName;
                $fileLinkdefault = 'upload/image-default.png';
                if (in_array($fileActualExt,$allowed)) {
                    if ($fileError === 0) {
                        if ($fileSize < 10000000) {
                            $fileNameNew = uniqid('', true).".".$fileActualExt;
                            define ('SITE_ROOT', dirname(__DIR__, 1));
                            $fileDestination = SITE_ROOT.'/'.$fileLink;
                            move_uploaded_file($fileTmpName, $fileDestination);
                        }
                        else {
                            $message = "Your file is too big!";
                        }
                    }
                    else {
                        $message = "There was an error uploading your file!";
                    }
                }
                if(!empty($fileName) && $fileName!=='') {
                    $query_up="UPDATE $table_prefix
                        SET firstname='{$firstname}',
                            lastname='{$lastname}',
                            phone='{$phone}',
                            email='{$email}', 
                            birthday='{$birthday}', 
                            avatar='{$fileLink}', 
                            is_active={$isactive},
                            updated_at={$updated}
                        WHERE id={$id} 
                    ";
                }
                else {
                    if (empty($avatar)) {
                        $query_up="UPDATE $table_prefix
                        SET firstname='{$firstname}',
                            lastname='{$lastname}',
                            phone='{$phone}',
                            email='{$email}', 
                            birthday='{$birthday}', 
                            avatar='{$fileLinkdefault}', 
                            is_active={$isactive},
                            updated_at={$updated}
                        WHERE id={$id} 
                    ";
                    }
                    else {
                        $query_up="UPDATE $table_prefix
                            SET firstname='{$firstname}',
                                lastname='{$lastname}',
                                phone='{$phone}',
                                email='{$email}', 
                                birthday='{$birthday}', 
                                avatar='{$avatar}', 
                                is_active={$isactive},
                                updated_at={$updated}
                            WHERE id={$id} 
                        ";
                    }
                }
                $results_up=mysqli_query($db->connect(),$query_up);
                if(!$results_up) {
                    die("MySQL error".mysqli_error($db->connect()));
                }
                if (mysqli_affected_rows($db->connect())==1) {
                    echo "<p class='required'>You have not fixed anything</p>";
                }
                else {
                    $query_laup = "SELECT * FROM $table_prefix WHERE id={$id}";
                    $result_laup = mysqli_query($db->connect(),$query_laup);
                    if(!$result_laup) {
                        die("MySQL error".mysqli_error($db->connect()));
                    }
                    if(mysqli_num_rows($result_laup)==1) {
                        $user_laup = mysqli_fetch_array($result_laup);
                    }
                }
            }
            else   
            {
                $query_up="UPDATE $table_prefix
                        SET firstname='{$firstname}',
                            lastname='{$lastname}',
                            phone='{$phone}',
                            email='{$email}', 
                            birthday='{$birthday}', 
                            avatar='{$avatar}', 
                            is_active={$isactive},
                            updated_at={$updated}
                        WHERE id={$id} 
                    ";
                        
                $results_up=mysqli_query($db->connect(),$query_up);
                if(!$results_up) {
                    die("MySQL error".mysqli_error($db->connect()));
                }
                if (mysqli_affected_rows($db->connect())==1) {

                    echo "<p class='required'>You have not fixed anything</p>";
                }
                else {
                    $query_laup = "SELECT * FROM $table_prefix WHERE id={$id}";
                    $result_laup = mysqli_query($db->connect(),$query_laup);
                    if(!$result_laup) {
                        die("MySQL error".mysqli_error($db->connect()));
                    }
                    if(mysqli_num_rows($result_laup)==1) {
                        $user_laup = mysqli_fetch_array($result_laup);
                    }
                }
            } 

        }
        else {
            $message="<p class='required'>Please enter your info full </p>";
        }
    }
?>
<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <section id="contents">
                    <div class="ql-user">
                        <h3>Edit User 
                        </h3>
                        <form id="formupdate" name="formupdate" method="post" action="edit_user.php" enctype="multipart/form-data">
                            <div class="message help-block with-errors"></div>
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" name="username" class="form-control username" value="<?php if(isset($user['fb_id'])) { echo $user['username']; } else { echo $username; } ?>" readonly="true">
                            </div>
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="firstname" class="form-control firstname" value="<?php if (isset($user_laup['firstname'])) {echo $user_laup['firstname']; } elseif (isset($user['fb_id'])) { echo $user['firstname']; } else { echo $firstname; } ?>">
                                <div class="help-block firstname with-errors"></div>
                                <?php if (isset($errors) && in_array('firstname',$errors)) {
                                    echo "<p class='required'>First Name field is required</p>";
                                } ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="lastname" class="form-control lastname" value="<?php if (isset($user_laup['lastname'])) {echo $user_laup['lastname']; } elseif (isset($user['fb_id'])) { echo $user['lastname']; } else { echo $lastname; } ?>">
                                <div class="help-block lastname with-errors"></div>
                                <?php if (isset($errors) && in_array('lastname',$errors)) {
                                    echo "<p class='required'>Last Name field is required</p>";
                                } ?>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="number" name="phone" class="form-control phone" value="<?php if (isset($user_laup['phone'])) {echo $user_laup['phone']; } elseif (isset($user['fb_id'])) { echo $user['phone']; } else { echo $phone; } ?>">
                                <div class="help-block phone with-errors"></div>
                                <?php if (isset($errors) && in_array('phone',$errors)) {
                                    echo "<p class='required'>Phone field is required</p>";
                                } ?>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control email" value="<?php if(isset($user['fb_id'])) { echo $user['email']; } else { echo $email; } ?>" readonly="true">
                                <div class="help-block email with-errors"></div>
                                <?php if (isset($errors) && in_array('email',$errors)) {
                                    echo "<p class='required'>Email field is required</p>";
                                } ?>
                            </div>
                            <div class="form-group">
                                <label>Birth Day</label>
                                <div class="input-group">
                                <input type="date" class="form-control datepickerbd" name="birthday" placeholder="yyyy.mm.dd" value="<?php if (isset($user_laup['birthday'])) {echo $user_laup['birthday']; } elseif (isset($user['fb_id'])) { echo $user['birthday']; } else { echo $birthday; } ?>">
                                <div class="help-block birthday with-errors"></div>
                                </div>
                                <span class="help-block required">
                                    <?php if (isset($errors) && in_array('birthday',$errors)) {
                                        echo "Birth Day field is required";
                                    } ?>
                                </span>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label>Avatar</label>
                                </div>
                                <?php 
                                    if (!empty($user['fb_id'])) {
                                ?>
                                <div class="col-sm-4">
                                    <img src="<?php if (isset($user['avatar'])) { echo $user['avatar']; }?>" alt="">
                                </div>
                                <?php
                                } 
                                else {
                                ?>
                                <div class="col-sm-4">
                                    <img id="avatarUser" src="<?php if (isset($user['avatar'])) { echo $user['avatar']; } elseif(isset($avatar)) { echo $avatar; } else { echo '../upload/image-default.png'; } ?>" alt="">
                                    <input type="hidden" name="img" value="<?php if (isset($user['avatar'])) { echo $user['avatar']; }?>">
                                </div>
                                <div class="col-sm-8">
                                    <input type="file" id="file" class="avatar" name="avatar" value="">
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                            <div class="form-group">
                                <?php 
                                	if(!empty($user_laup['is_active'])) {
                                		if($user_laup['is_active']==1) {
                                			?>
	                                		<label style="display: block">Status</label>
		                                    <label class="radio-inline"><input checked="checked" type="radio" class="isactive" name="isactive" value="1">Active</label>
		                                    <label class="radio-inline"><input type="radio" class="isactive" name="isactive" value="0">Deactive</label> 
	                                		<?php
                                		}
                                		else {
                                			?>
	                                		<label style="display: block">Status</label>
		                                    <label class="radio-inline"><input type="radio" class="isactive" name="isactive" value="1">Active</label>
		                                    <label class="radio-inline"><input checked="checked" type="radio" class="isactive" name="isactive" value="0">Deactive</label> 
	                                		<?php
                                		}
                                	}
                                	elseif(isset($user['is_active']))
                                	{
                                		if ($user['is_active']==1) {
                                			?>
		                                    <label style="display: block">Status</label>
		                                    <label class="radio-inline"><input checked="checked" type="radio" class="isactive" name="isactive" value="1">Active</label>
		                                    <label class="radio-inline"><input type="radio" class="isactive" name="isactive" value="0">Deactive</label> 
		                                    <?php
                                		}
                                		else {
                                			?>
		                                    <label style="display: block">Status</label>
		                                    <label class="radio-inline"><input type="radio" class="isactive" name="isactive" value="1">Active</label>
		                                    <label class="radio-inline"><input checked="checked" type="radio" class="isactive" name="isactive" value="0">Deactive</label> 
		                                    <?php
                                		}
                                	}
	                            ?>
                            </div>
                            <input type="hidden" name="id" value="<?php if(isset($user['id'])) { echo $user['id']; } ?>">
                            <input name="submit" type="submit" id="addRecord" class="btn btn-primary" value="Update">
                        </form>
                    </div>
                </section>
            </div>
        </div>
        
    </div>
</div>
<?php include('templates/footer.php'); ?>