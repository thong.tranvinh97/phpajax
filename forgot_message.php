<?php 
	include('includes/header.php');
?>
	<div class="register bg-login">
		<div class="container">
			<div class="thankyou">
				<div class="container">
					<div class="row">
						<h4>Message has been sent. Please check email</h4>
						<i class="fa fa-envelope-o" aria-hidden="true"></i>
						<a href="login.php" class="back" title=""><i class="fa fa-long-arrow-left" aria-hidden="true"></i>Back</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include('includes/footer.php'); ?>