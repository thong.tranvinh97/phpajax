<?php 
	include('includes/header.php');
	include('admin/templates/inc/myconnect.php');
	include('admin/templates/inc/functions.php');

	if (isset($_GET['key_forgot'])) {
		$key_forgot = $_GET['key_forgot'];
		$query = "SELECT * FROM $table_prefix WHERE key_forgot='{$key_forgot}' AND is_active=1 ";
		$result = mysqli_query($db->connect(),$query);
		$user = $result->fetch_object();
		$id = $user->id;
		kt_query($result,$query);
		if (mysqli_num_rows($result)==1) {
			?>
			<div class="reset-pass bg-login">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<div class="reset-password wrap-login">	
								<h2 class="title">Create New Password</h2>
								<?php
									if($_SERVER['REQUEST_METHOD']=='POST') {  
										$errors = array();
										if (empty($_POST['password'])) {
											$errors[]='password';
										}
										else {
											$password=md5(trim($_POST['password']));
										}
										if (trim($_POST['confirm-password'])!=trim($_POST['password'])) {
											$errors[]='confirm-password';
										}
										if(empty($errors)) { 
											$query2 = "UPDATE $table_prefix SET password='{$password}', key_forgot='' WHERE id={$id} ";
											$result2 = mysqli_query($db->connect(),$query2);
											if ($result2) {
												echo "
												<p style='color: #53ef53f0;'>Your password has been change. You my now login</p>
												<a href='login.php' title=''>Back</a>
												";

											}
											else {
												echo mysqli_error($db->connect());
											}
										}
									}
								?>	
								<form class="frmrspass" name="frmrspass" method="POST">
									<div class="form-group">
										<label>New Password</label>
										<input type="password" name="password" class="form-control" placeholder="Enter Password">
										<?php if (isset($errors) && in_array('password',$errors)) {
											echo "<p class='required'>Password field is required</p>";
										} ?>
									</div>
									<div class="form-group">
										<label>Confirm New Password</label>
										<input type="password" name="confirm-password" class="form-control" placeholder="Enter Confirm Password">
										<?php if (isset($errors) && in_array('confirm-password',$errors)) {
											echo "<p class='required'>Passwords should be same</p>";
										} ?>
									</div>
									<button type="submit" class="btn btn-primary">Submit</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
		else 
		{
			?>
			<div class="reset-pass bg-login">
				<div class="container">
					<div class="reset-password">
						<h4 class='required'>This account invalid or already verification</h4>
						<a href='login.php' class="back" title=''><i class='fa fa-long-arrow-left' aria-hidden='true'></i>Back</a>
					</div>
				</div>
			</div>
			<?php
		}
	}
	else {
		die("Something went wrong");	
	} 

include('includes/footer.php'); 
?>
