<?php 
	include ('includes/header.php'); 
	require_once __DIR__.'/vendor/autoload.php';
?>
	<div class="register bg-login">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="wrap-login">
						<h2 class="title">Register</h2>
						<?php  
							include('admin/templates/inc/myconnect.php');
							include('admin/templates/inc/functions.php');
							if($_SERVER['REQUEST_METHOD']=='POST') {
								$errors = array();
								if (empty($_POST['username'])) {
									$errors[]='username';
								}
								else {
									$username=$_POST['username'];
								}
								if (empty($_POST['password'])) {
									$errors[]='password';
								}
								else {
									$password=md5(trim($_POST['password']));
								}
								if (trim($_POST['confirm-password'])!=trim($_POST['password'])) {
									$errors[]='confirm-password';
								}
								if (empty($_POST['firstname'])) {
									$errors[]='firstname';
								}
								else {
									$firstname=$_POST['firstname'];
								}
								if (empty($_POST['lastname'])) {
									$errors[]='lastname';
								}
								else {
									$lastname=$_POST['lastname'];
								}
								if (empty($_POST['phone'])) {
									$errors[]='phone';
								}
								else {
									$phone=$_POST['phone'];
								}
								if (filter_var(($_POST['email']),FILTER_VALIDATE_EMAIL)==TRUE) {
									$email=mysqli_real_escape_string($db->connect(),$_POST['email']);
								}
								else {
									$errors[]='email';
								}
								if (empty($_POST['birthday'])) {
									$errors[]='birthday';
								}
								else {
									$birthday=$_POST['birthday'];
								}
								$avatar = 'upload/image-default.png';
								if (empty($errors)) {
									$query="SELECT username FROM $table_prefix WHERE username='{$username}' ";
									$results=mysqli_query($db->connect(),$query);kt_query($results,$query);
									$query2="SELECT email FROM $table_prefix WHERE email='{$email}'";
									$results2=mysqli_query($db->connect(),$query2);kt_query($results2,$query2);
									if (mysqli_num_rows($results)==1) {
										$message="<p class='required'>Account already exists</p>";
									}
									elseif (mysqli_num_rows($results2)==1)
									{
										$message="<p class='required'>Email already exists</p>";
									}
									else {
										$created = date("Y/m/d");
										$vkey = md5(time());
										$query_in="INSERT INTO $table_prefix (username, password, firstname, lastname, phone, email, birthday, role, avatar, fb_id, key_active, key_forgot, is_active, is_deleted, created_at, updated_at) VALUES ('{$username}','{$password}',N'{$firstname}',N'{$lastname}','{$phone}','{$email}','{$birthday}','0','{$avatar}','','{$vkey}','','0','0','{$created}','')";
										$results_in=mysqli_query($db->connect(),$query_in);
										kt_query($results_in, $query_in);
										if(mysqli_affected_rows($db->connect())==1) {
											echo "<p class='required'>Registration failed</p>";
										}
										else {
											echo "<p style='color: #53ef53f0;'>Successful registration. Please activate your account</p>";
											$mail = new PHPMailer\PHPMailer\PHPMailer(true);
											try {
										    //Server settings
										    // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
										    $mail->isSMTP();                                            // Send using SMTP
										    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
										    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
										    $mail->Username   = 'test.adsplc.97@gmail.com';                     // SMTP username
										    $mail->Password   = 'cubfclpkcfrzynpr';                               // SMTP password
										    // $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
										    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

										    //Recipients
										    $mail->setFrom('test.adsplc.97@gmail.com', 'ADMIN');
										    $mail->addAddress($email, $username);     // Add a recipient
										    // $mail->addAddress('ellen@example.com');               
										    // Name is optional
										    $mail->addReplyTo('test.adsplc.97@gmail.com', 'Information');
										    // $mail->addCC('cc@example.com');
										    // $mail->addBCC('bcc@example.com');

										    // Attachments
										    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
										    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

										    // Content
										    $mail->isHTML(true);                                  // Set email format to HTML
										    $mail->Subject = "Email Verification";
										    $mail->Body    = "<a href='https://thongtv.lahvui.xyz/phpcanban/active.php?vkey=$vkey'>Active Account</a>";
										    $mail->AltBody = "This is the body in plain text for non-HTML mail clients";

										    $mail->send();
										    echo '<p style="color: #53ef53f0">Message has been sent</p>';
											} catch (Exception $e) {
											    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
											}
											header('Location: thankyou.php');
										}
									}
								}
								else {
									$message="<p class='required'>Please enter your info full</p>";
								}
							}
						?>
						<form class="frmregister" name="frmregister" method="POST" data-toggle="validator" novalidate="true">
							<?php if (isset($message)) {
								echo $message;
							} ?>
							<div class="form-group">
								<label>Username</label>
								<input type="text" name="username" class="form-control" data-error="The username is required and cannot be empty" value="<?php if (isset($username)) { echo $username; }?>">
								<span class="help-block required">
									<?php if (isset($errors) && in_array('username',$errors)) {
										echo "Username field is required";
									} ?>
								</span>
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" name="password" data-minlength="6" class="form-control">
								<span class="help-block required">
									<?php if (isset($errors) && in_array('password',$errors)) {
										echo "Password field is required";
									} ?>
								</span>
							</div>
							<div class="form-group">
								<label>Confirm Password</label>
								<input type="password" name="confirm-password" class="form-control">
								<span class="help-block required">
									<?php if (isset($errors) && in_array('confirm-password',$errors)) {
										echo "Passwords should be same";
									} ?>
								</span>
							</div>
							<div class="form-group">
								<label>First Name</label>
								<input type="text" name="firstname" class="form-control" value="<?php if (isset($firstname)) { echo $firstname; }?>">
								<span class="help-block required">
									<?php if (isset($errors) && in_array('firstname',$errors)) {
										echo "First Name field is required";
									} ?>
								</span>
							</div>
							<div class="form-group">
								<label>Last Name</label>
								<input type="text" name="lastname" class="form-control" value="<?php if (isset($lastname)) { echo $lastname; }?>">
								<span class="help-block required">
									<?php if (isset($errors) && in_array('lastname',$errors)) {
										echo "Last Name field is required";
									} ?>
								</span>
							</div>
							<div class="form-group">
								<label>Phone</label>
								<input type="number" name="phone" class="form-control" value="<?php if (isset($phone)) { echo $phone; }?>">
								<span class="help-block required">
									<?php if (isset($errors) && in_array('phone',$errors)) {
										echo "Phone field is required";
									} ?>
								</span>
							</div>
							<div class="form-group">
								<label>Email</label>
								<input type="email" name="email" class="form-control" value="<?php if (isset($email)) { echo $email; }?>">
								<span class="help-block required">
									<?php if (isset($errors) && in_array('phone',$errors)) {
										echo "Email field is required";
									} ?>
								</span>
							</div>
							<div class="form-group">
								<label>Birth Day</label>
								<div class="input-group">
					                <input type="date" class="form-control datepickerbd" name="birthday" placeholder="yyyy.mm.dd" value="<?php if (isset($birthday)) { echo $birthday; }?>">
				              	</div>
				              	<span class="help-block required">
				              		<?php if (isset($errors) && in_array('phone',$errors)) {
										echo "Birth Day field is required";
									} ?>
				              	</span>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Register</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include('includes/footer.php'); ?>
